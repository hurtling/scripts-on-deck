#!/bin/bash
if [[ "$1"  == "on" ]] ; then toggle=1
elif [[ "$1" == "off" ]] ; then toggle=0
else
    echo "incorrect arguments. Specify 'on' or 'off'."
    exit
fi

pw=$(cat ~/p )
for n in {1..5} ; do
    { echo "$pw" ; echo "$toggle" ; } | sudo -kS -p "" tee /sys/devices/system/cpu/cpu$n/online >/dev/null
done
