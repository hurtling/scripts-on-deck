#!/bin/bash

# creates a text file listing all the games installed on the inserted SD card.
# the text file is labeled with the 'serial' of the SD card,
# which is different to all the numbers physically printed on the SD card itself,
# but which is hopefully still a unique ID

# get the mount point of the SD card
# this is usually /run/media/mmcblk0p1/steamapps/common ,
# but it's different if the SD card has a label
mountpointarray=($(mount | grep mmcblk))
mountpoint=${mountpointarray[2]}

if [ -z "$( mount | grep mmcblk )" ] ; then
    zenity --error --width=800  --text="Couldn't find mount point! Is the SD card mounted?" 2>/dev/null
    exit 1
fi

declare -A serials=(
    ["0x3584d957"]="022461100990"
    ["0x384433ca"]="022511208790"
    ["0xa5e79349"]="sandisk-slow"
    ["0x32416095"]="ps2-games"
    ["0x30560bd1"]="terry-bite"
)

linuxserial=$( cat /sys/block/mmcblk0/device/serial )
serial="${serials[$linuxserial]}"

if [ -z $serial ] ; then
    zenity --error --width=800 --text="Couldn't find serial number of SD card $linuxserial !" 2>/dev/null
    exit 1
fi

depth=1
#if this is an SD card containing steam games, list that.
if [ -d "$mountpoint/steamapps/common" ] ; then
    echo normal
    dudir="${mountpoint}/steamapps/common"
elif [ $serial == "ps2-games" ] ; then # if this is the ps2 games card
    echo ps2
    depth=2
    dudir=${mountpoint}/ps2/games
else #elsewise, just list the root dirs
    echo elsewise
    dudir="$mountpoint"
fi
# ls -1 --quoting-style=literal $mountpoint | grep -v Steam.dll > ${serial}-sd-games.txt
du -ahd $depth $dudir | sort -h | grep -v Steam.dll | sed "s|\t$dudir/|    |" > sd-card-contents/${serial}.txt

# get free space on SD card
spacearray=($(df -h | grep mmcblk))
space=${spacearray[3]}

echo -e "\nfree space: $space" >> sd-card-contents/${serial}.txt

# push changes to gitlab
git add -u
git commit -m "auto-commit changes to SD card catalogue"
commitresult=$?
git push
pushresult=$?

zenity --info --title="Success" --width=800 --text="space on SD '$serial': $space\ncommit result: $commitresult\npush result: $pushresult"
