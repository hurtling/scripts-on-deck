#!/bin/bash

# find which sd card (if any) a game is on
# allows the user to search which text file listing games on an sd card contains the entered string.

search=$(kdialog --title "srch" --inputbox "search for game")
RESULT=$?
if [ $RESULT = 0 ] ; then
    output=$(grep -Ii "$search" sd-card-contents/*.txt)
    zenity --info --title="Result" --width=800 --text="search results for '$search':\n$output"
fi
