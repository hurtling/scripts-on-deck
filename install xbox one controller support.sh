# written on 25 aug 2023

# installs xbox one controller kernel module driver
# according to the second reddit link, the installation will be wiped after every steam deck OS update (beta or stable)
# so this is a quicker way to do the steps again

# NOTE: this is a more a set of steps to be inspected and run manually,
# not a robust enough script to be run automatically each time (you'll probably be fine but still)

# from https://old.reddit.com/r/SteamDeck/comments/vz19fp/xone_installation_for_steam_deck_guide_xbox/
sudo steamos-readonly disable
sudo pacman-key --init
sudo pacman-key --populate archlinux
sudo pacman -Syu curl wget git base-devel gcc cabextract linux-neptune-headers

# from https://old.reddit.com/r/SteamDeck/comments/ztq9j7/dkms_support_after_34_update/
sudo pacman -Syu dkms

# from https://github.com/medusalix/xone
#git clone https://github.com/medusalix/xone
#cd xone
sudo ./install.sh --release

# if you get
# Error! Your kernel headers for kernel 5.13.0-valve37-1-neptune cannot be found at
# then do the following
sudo pacman -S linux-neptune-61-headers
# then re-run the previous commmand
