
import sys, os, re
from os.path import join, getsize, islink
import xxhash

#ignore groups whose files add up to less than this
MIN_GROUP_TOTAL = 1048576 # 1MB

# prints the passed filesize-in-bytes (eg 12345678) in a human-readable value
def sizeof_fmt(num, suffix="B"):
    for unit in ("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"):
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"

nametopath={}
sizetoname={}
# recursively for all files and directories in the supplied directory (supplied as an argument):
for dirpath, dirnames, filenames in os.walk(sys.argv[1], topdown=True, onerror=None, followlinks=False):
    for name in filenames:
        # filter only files whose extension matches
        if re.search(r'\.(exe|dll|msi|nls|cab)$', name, re.IGNORECASE) is not None:
            fullpath=join(dirpath, name)
            if islink(fullpath):
                continue
            #stat = os.stat(fullpath, follow_symlinks=False)
            # record every file size in bytes
            key = getsize(join(dirpath, name))
            # ignore empty files (size of 0)
            if key > 0:
                nametopath.setdefault(name, [])
                nametopath[name].append(dirpath)

                sizetoname.setdefault(key, [])
                sizetoname[key].append(join(dirpath, name))

# print("files considered:")
# for name, paths in nametopath.items():
#     if len(paths) > 1:
#         print(name+": "+str(len(paths)))
#
# print()
print("group sizes:")
groups = 0
total_files = 0
total_you_could_save = 0
for size, files in sizetoname.items():
    if len(files) > 1 and (size * len(files) > MIN_GROUP_TOTAL):
        groups += 1
        total_files += len(files)
        total = size * (len(files)-1)
        total_you_could_save += total
        print(files[0]+": "+str(len(files))+" files totalling "+sizeof_fmt(total))
print(str(groups)+" total groups to analyze further, totalling "+str(total_files)+" files")
print("saving if all are valid dupe groups: "+sizeof_fmt(total_you_could_save))

print("hashing the first 4kb of all files...")
name_to_4kb_hash={}
# ok now we've populated the dict,
for size, files in sizetoname.items():
    # for every remaining file whose size is the same as another file
    # (there can't be any duplicates if nothing else is the same size)
    # AND the number of files * the size of each file is > 10MB (don't bother with few and/or small files)
    if len(files) > 1 and (size * len(files) > MIN_GROUP_TOTAL):
        ##print("identically-sized files that add up to >10MiB:")
        #for filename in files:
            #print(filename)
        #print("total:"+sizeof_fmt(size * len(files)))
        # calculate the hash of the first 4kb of each file,
        # and store them in a dict<hash, list<filename>>
        # use xxhash btw! (or another non-cryptographic hash function, if research finds one)
        for filename in files:
            file = open(filename, "rb")
            first_4kb = file.read(4096)
            key = xxhash.xxh3_64_hexdigest(first_4kb)
            file.close()
            name_to_4kb_hash.setdefault(key, [])
            name_to_4kb_hash[key].append(filename)

total_files_remaining = 0
for hash, filenames in name_to_4kb_hash.items():
    total_files_remaining += len(filenames)
print(str(total_files_remaining)+" files hashed, which are left to analyse")

# for any files with a size AND 4kb-hash identical to another:
# calculate the full hash
print("calculating the full hash of all uneliminated files...")
name_to_full_hash = {}
for hash, filenames in name_to_4kb_hash.items():
    if len(filenames) > 1:
        # calculate the hash of the whole file
        for filename in filenames:
            fyl = open(filename, "rb")

            x = xxhash.xxh3_64()
            bytes_to_read = 50 * 1024 * 1024
            bytes_red = bytes_to_read

            while bytes_red == bytes_to_read:
                ten_meg_chunk = fyl.read(bytes_to_read)
                x.update(ten_meg_chunk)
                bytes_red = len(ten_meg_chunk)

            fyl.close()
            key = x.hexdigest()
            name_to_full_hash.setdefault(key, [])
            name_to_full_hash[key].append(filename)

fullhashed_files = 0
for fullhash, filenames in name_to_full_hash.items():
    if len(filenames) > 1 and (size * len(filenames) > MIN_GROUP_TOTAL):
        fullhashed_files += len(filenames)
print("fully-hashed files in groups with >1 member, and which add up to >1MB:"+str(fullhashed_files))
# if the full hashes are still the same, then:
    # IMPL CHOICE:
        # either do a bytewise comparison of the whole of each pair of files (expensive, unnecessary??)
        # or THEY ARE IDENTICAL!
    # check UNIX permission bits are the same across all the identical files
    # add all but the oldest file to the list of files we would replace with a hardlink (path relative to argument, size, unix permissions)
# print every file that would be replaced with a hardlink, in a table row of the format described above
# print the total size (in human-readable format) that could be saved by deleting the duplicates
# IMPL CHOICE:
    # FOR EVERY FILE ask whether to delete it and replace with a hardlink
    # FOR EVERY GROUP OF DUPLICATES ("dupe group" :P) ask
    # ASK ONCE to delete all the groups in the entire run.
# create the hardlink immediately after each file deletion. 
# rename the file to be deleted
# if this succeeds,
    # create the hardlink
    # if hardlink creation succeeded, delete the file
# if renaming fails, we probably don't have permissions to delete either, so fail out
# If the deletion or the hardlinking fail, revert by copying the file back

# check it's not a softlink!

# if file is already a hardlink, check if, for all the bytewise duplicates (once we know them), there is >1 inode between all of them.
# if there is, we can still save space. if not, we can't

# we don't have a way to just check if a file is a hardlink, because *all* files are hardlinks.
# so we just need to find files that refer to the same inode as any other file
# can we reduce the search space by finding any inodes with >1 reference? I'd wager yes
