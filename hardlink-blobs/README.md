after an evening of reading around, the 2 tools we really want are venv and pip.
This blogpost really helpfully explains the messy XKCD927 situation on Python with package/env managers:
<https://alpopkes.com/posts/python/packaging_tools/>

On Steam Deck, the default installation of Python doesn't come with pip.
Installing it via pacman (as I probably did before) will get erased eventually.

BUT, it does come with venv.
so the solution is to use the ven environment (which we'll need in order to install xxhash anyway)
in order to get a copy of pip:

    # create the virtual environment
    python3 -m venv hardlinkblobs

    #activate the virtual environment
    source hardlinkblobs/bin/activate

when I got this successfully running, I next ran the `get-pip.py` script from
<https://bootstrap.pypa.io/get-pip.py> as described in <https://pip.pypa.io/en/stable/installation/>,
but upon running that script it said it was replacing an older version of pip,
so you might just be able to use that, without bothering installing the newer one.


    # install our dependencies, which at time of writing is just xxhash
    pip install -r requirements.txt

    #...later, deactivate the virtual environment, as described in the blot post above
    deactivate
