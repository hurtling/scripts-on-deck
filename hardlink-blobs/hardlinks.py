import os, sys
for dirpath, dirnames, filenames in  os.walk(sys.argv[1], topdown=True, onerror=None, followlinks=False):
    for name in filenames:
        fullpath=os.path.join(dirpath, name)
        stat = os.stat(fullpath)
        if stat.st_nlink > 1:
            print(fullpath)